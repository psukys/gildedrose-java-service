package com.gildedrose;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class GildedRoseTest {

    private Item foo,
                 brie,
                 concert,
                 sulfuras,
                 conjured;

    @Before
    public void setUp() {
        foo = new Item("foo", 10, 10);
        brie = new Item("Aged Brie", 10, 10);
        concert = new Item("Backstage passes to a TAFKAL80ETC concert", 10, 10);
        sulfuras = new Item("Sulfuras, Hand of Ragnaros", 10, 80);
        conjured = new Item("Conjured item", 10, 10);
    }
    /**
     * Tests that SellIn degrades after each update
     * Given: a casual item with mid values
     * Expect: SellIn to be decreased by 1
     */
    @Test
    public void testSellInDegrade() {
        int sellIn = 10;
        foo.sellIn = sellIn;
        GildedRose target = new GildedRose(new Item[]{foo});
        target.updateQuality();
        assertEquals(sellIn - 1, foo.sellIn);
    }

    /**
     * Tests that Quality degrades after each update
     * Given: a casual item with mid values
     * Expect: Quality to be decreased by 1
     */
    @Test
    public void testQualityDegrade() {
        int quality = 10;
        foo.quality = quality;
        GildedRose target = new GildedRose(new Item[]{foo});
        target.updateQuality();
        assertEquals(quality - 1, foo.quality);
    }
    /**
     * Tests that Quality of Brie increases after each update
     * Given: Aged Brie with non-max Quality
     * Expect: Quality to be increased by 1
     */
    @Test
    public void testBrieQualityIncrease() {
        int quality = 10;
        brie.quality = quality;
        GildedRose target = new GildedRose(new Item[]{brie});
        target.updateQuality();
        assertEquals(quality + 1, brie.quality);
    }

    /**
     * Tests that quality of Brie increases by 2 when it's expired
     * Given: Aged Brie with non max quality and 0 sellin
     * Expect: After update - quality increases by 2
     */
    @Test
    public void testExpiredBrieDoubleQualityIncrease() {
        int quality = 10;
        int sellIn = 0;
        brie.quality = quality;
        brie.sellIn = sellIn;
        GildedRose target = new GildedRose(new Item[]{brie});
        target.updateQuality();
        assertEquals(quality + 2, brie.quality);
    }

    /**
     * Tests that Sulfuras has persistant quality
     * Given: Sulfuras item
     * Expect: Quality persists to be 80 after updates
     */
    @Test
    public void testSulfurasQualityPersistance() {
        GildedRose target = new GildedRose(new Item[]{sulfuras});
        target.updateQuality();
        assertEquals(80, sulfuras.quality);
    }

    /**
     * Tests that quality cannot go over 50 for Brie
     * Given: Aged Brie item with 50 quality
     * Expect: After update, quality stays 50
     */
    @Test
    public void testQualityMaxWithBrie() {
        brie.quality = 50;
        GildedRose target = new GildedRose(new Item[]{brie});
        target.updateQuality();
        assertEquals(50, brie.quality);
    }

    /**
     * Tests that quality cannot go over 50 for concert backpass
     * Given: Concert tickets with 50 quality
     * Expect: After update, quality stays 50
     */
    @Test
    public void testQualityMaxWithConcert() {
        concert.quality = 50;
        GildedRose target = new GildedRose(new Item[]{concert});
        target.updateQuality();
        assertEquals(50, concert.quality);
    }
    
    /**
     * Tests that quality for a concert with less than 50 for concert pass stops at 50,
     * when update step is higher
     * Given: Concert tickets with 49 quality, sellin <= 10
     * Expect: After update, quality tops to 50
     */
    @Test
    public void testQualityMaxWithConcertOverflow() {
        concert.quality = 49;
        concert.sellIn = 10;
        GildedRose target = new GildedRose(new Item[]{concert});
        target.updateQuality();
        assertEquals(50, concert.quality);
    }

    /**
     * Tests that quality of normal item degradation
     * Given: Normal item with 0 quality
     * Expect: After update, quality stays 0
     */
    @Test
    public void testQualityMin() {
        foo.quality = 0;
        GildedRose target = new GildedRose(new Item[]{foo});
        target.updateQuality();
        assertEquals(0, foo.quality);
    }

    /**
     * Tests that quality of normal item degradation when expired
     * Given: Normal item with 0 quality and 0 sellin
     * Expect: After update, quality stays 0
     */
    @Test
    public void testQualityMinExpired() {
        foo.sellIn = 0;
        foo.quality = 0;
        GildedRose target = new GildedRose(new Item[]{foo});
        target.updateQuality();
        assertEquals(0, foo.quality);
    }

    /**
     * Tests that quality of a normal expired item degrades down to 0 and no less
     * Given: normal item with 1 quality and 0 sellin (-2 quality per update)
     * Expect: After update, quality is 0
     */
    @Test
    public void testQualityMinExpiredUnderflow() {
        foo.sellIn = 0;
        foo.quality = 1;
        GildedRose target = new GildedRose(new Item[]{foo});
        target.updateQuality();
        assertEquals(0, foo.quality);
    }
    
    /**
     * Tests that quality of a concert ticket increases
     * Given: Concert ticket with some quality, and selling > 10 days
     * Expect: After update, quality increases by 1
     */
    @Test
    public void testConcertNormalUpdate() {
        int quality = 10;
        concert.quality = quality;
        concert.sellIn = 50;
        GildedRose target = new GildedRose(new Item[]{concert});
        target.updateQuality();
        assertEquals(quality + 1, concert.quality);
    }

    /**
     * Tests that quality increase by double if less-eq than 10 days are left to sell.
     * Given: Concert ticket with some quality, and 5 < sellin <= 10 days
     * Expect: After update, quality increases by 2
     */
    @Test
    public void testConcert10dayUpdate() {
        int quality = 10;
        concert.quality = quality;
        concert.sellIn = 10;
        GildedRose target = new GildedRose(new Item[]{concert});
        target.updateQuality();
        assertEquals(quality + 2, concert.quality);
    }

    /**
     * Tests that quality increase by double if less-eq than 5 days are left to sell.
     * Given: Concert ticket with some quality, and 0 < sellin <= 5 days
     * Expect: After update, quality increases by 3
     */
    @Test
    public void testConcert5dayUpdate() {
        int quality = 10;
        concert.quality = quality;
        concert.sellIn = 5;
        GildedRose target = new GildedRose(new Item[]{concert});
        target.updateQuality();
        assertEquals(quality + 3, concert.quality);
    }

    /**
     * Tests that concert pass is worthless after the concert
     * Given: Concert ticket with some quality and 0 sellin
     * Expect: After update, quality is 0
     */
    @Test
    public void testConcert0dayUpdate() {
        concert.quality = 10;
        concert.sellIn = 0;
        GildedRose target = new GildedRose(new Item[]{concert});
        target.updateQuality();
        assertEquals(0, concert.quality);
    }

    /**
     * Tests that aged brie continues to increase quality by 2 with negative sellin
     * Given: Aged Brie with negative SellIn
     * Expect: upon update quality increases by 2
     */
    @Test
    public void testBrieNegativeSellIn() {
        int quality = 10;
        brie.quality = quality;
        brie.sellIn = -1;
        GildedRose target = new GildedRose(new Item[]{brie});
        target.updateQuality();
        assertEquals(quality + 2, brie.quality);
    }

    /**
     * Tests that normal item continues to decrease quality by 2 with negative sellin
     * Given: normal item with negative SellIn
     * Expect: upon update quality increases by 2
     */
    @Test
    public void testNormalNegativeSellIn() {
        int quality = 10;
        foo.quality = quality;
        foo.sellIn = -1;
        GildedRose target = new GildedRose(new Item[]{foo});
        target.updateQuality();
        assertEquals(quality - 2, foo.quality);
    }

    /**
     * Test that conjured item quality decreases 2x
     * Given: some conjured item
     * Expect: upon update quality decreases by 2
     */
    @Test
    public void testConjuredItemQuality() {
        int quality = 10;
        conjured.quality = quality;
        GildedRose target = new GildedRose(new Item[]{conjured});
        target.updateQuality();
        assertEquals(quality - 2, conjured.quality);
    }

    /**
     * Test that outdated conjured item quality decreases 4x
     * Given: expired conjured item
     * Expect: upon update quality drops by 4
     */
    @Test
    public void testExpiredConjuredItemQuality() {
        int quality = 10;
        int sellIn = 0;
        conjured.quality = quality;
        conjured.sellIn = sellIn;
        GildedRose target = new GildedRose(new Item[]{conjured});
        target.updateQuality();
        assertEquals(quality - 4, conjured.quality);
    }
}
