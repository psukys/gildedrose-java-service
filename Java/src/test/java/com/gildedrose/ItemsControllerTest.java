package com.gildedrose;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ItemsControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ItemRepository itemRepository;

    @Before
    public void setUp() {
        this.itemRepository.deleteAll();
    }

    /**
     * Given that there are no items, expect empty list
     * @throws Exception spring related exceptions
     */
    @Test
    public void testEmptyOutput() throws Exception {
        this.mockMvc.perform(get("/items")).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content").isArray())
                .andExpect(jsonPath("$.content").isEmpty());
    }

    /**
     * Given: one item added
     * Expect: that on item available
     * @throws Exception spring related exceptions
     */
    @Test
    public void testOneItem() throws Exception {
        ItemBean item = new ItemBean();
        this.itemRepository.save(item);
        this.mockMvc.perform(get("/items")).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content[0].name").value(item.getName()))
                .andExpect(jsonPath("$.content[0].sellIn").value(item.getSellIn()))
                .andExpect(jsonPath("$.content[0].quality").value(item.getQuality()));
    }

    /**
     * Given: outdated item
     * Expect: update to be issued
     */
    @Test
    public void testOneItemUpdate() throws Exception {
        LocalDate outdatedDate = LocalDate.now().minus(1, ChronoUnit.DAYS);
        String expectedDate = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        ItemBean item = new ItemBean(new Item("Dummy", 1, 1), outdatedDate);
        this.itemRepository.index(item);
        this.mockMvc.perform(get("/items/update")).andDo(print())
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[0].name").value(item.getName()))
            .andExpect(jsonPath("$[0].sellIn").value(item.getSellIn() - 1))
            .andExpect(jsonPath("$[0].quality").value(item.getQuality() - 1))
            .andExpect(jsonPath("$[0].lastUpdate").value(expectedDate));
    }


    /**
     * Given: two outdated items and one up-to-date
     * Expect: two outdated items to be updated
     */
    @Test
    public void testSomeItemUpdate() throws Exception {
        LocalDate outdatedDate = LocalDate.now().minus(1, ChronoUnit.DAYS);
        ItemBean firstOutdated = new ItemBean(new Item("first outdated", 10, 10), outdatedDate);
        firstOutdated.setId(1L);
        ItemBean secondOutdated = new ItemBean(new Item("second outdated", 10, 10), outdatedDate);
        secondOutdated.setId(2L);
        ItemBean firstUptodate = new ItemBean(new Item("first uptodate", 10, 10), LocalDate.now());
        firstUptodate.setId(3L);
        itemRepository.index(firstUptodate);
        itemRepository.index(firstOutdated);
        itemRepository.index(secondOutdated);
        

        this.mockMvc.perform(get("/items/update")).andDo(print())
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.length()").value(2));
    }
}