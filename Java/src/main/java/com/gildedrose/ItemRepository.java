package com.gildedrose;

import com.gildedrose.ItemBean;

import org.springframework.data.domain.Page;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface ItemRepository extends ElasticsearchRepository<ItemBean, Long> {
    Page<ItemBean> findAll();
} 