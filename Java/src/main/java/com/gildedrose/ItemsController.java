package com.gildedrose;

import java.util.List;
import java.util.stream.Collectors;
import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ItemsController {

    @Autowired
    ItemRepository itemRepository;

    @RequestMapping("/items")
    public Page<ItemBean> items() {
        return itemRepository.findAll();
    }

    @RequestMapping("/items/update")
    public List<ItemBean> update() {
        List<ItemBean> items = itemRepository.findAll()
                .getContent()
                .stream()
                .filter(item -> item.getLastUpdate().compareTo(LocalDate.now()) < 0)
                .collect(Collectors.toList());

        GildedRose gildedRose = new GildedRose(items);
        gildedRose.updateQuality();
        for (ItemBean item: items) {
            item.setLastUpdate(LocalDate.now());
            itemRepository.save(item);
        }
        return items;
    }
}