package com.gildedrose;

import java.time.LocalDate;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.gildedrose.Item;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * Decorator pattern, since Item class cannot be touched.
 */
@Document(
    indexName = "item",
    shards = 1,
    replicas = 0,
    refreshInterval = "-1")
public class ItemBean {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name; //dummy values, don't know how to enforce using Item (parsing back fails)
    private int sellIn;
    private int quality;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @JsonSerialize(using = ToStringSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate lastUpdate;
    private Item item;

    public ItemBean() {
        this(new Item("", 0, 0), LocalDate.now());
    }

    public ItemBean(Item item) {
        this(item, LocalDate.now());
    }

    public ItemBean(Item item, LocalDate lastUpdate) {
        this.item = item;
        this.lastUpdate = lastUpdate;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return this.id;
    }

    public void setName(String name) {
        this.item.name = name;
    }

    public String getName() {
        return this.item.name;
    }
    public void setSellIn(int sellIn) {
        this.item.sellIn = sellIn;
    }

    public int getSellIn() {
        return this.item.sellIn;
    }

    public void setQuality(int quality) {
        this.item.quality = quality;
    }

    public int getQuality() {
        return this.item.quality;
    }

    public void setLastUpdate(LocalDate date) {
        this.lastUpdate = date;
    }

    public LocalDate getLastUpdate() {
        return this.lastUpdate;
    }

    @Override
	public String toString() {
		return String.format("Item[id=%l, name='%s', sellin='%s', quality='%s']", this.id,
				this.item.name, this.item.sellIn, this.item.quality);
    }
}