package com.gildedrose;

import java.util.ArrayList;
import java.util.List;

public class GildedRose {
    List<ItemBean> items;

    public GildedRose(Item[] items) {
        this.items = new ArrayList<ItemBean>();
        for (Item item: items) {
            this.items.add(new ItemBean(item));
        }
    }

    public GildedRose(List<ItemBean> items) {
        this.items = items;
    }

    /**
     * Updates Aged Brie item.
     * @param item instance of Aged Brie
     */
    protected void updateAgedBrie(ItemBean item) {
        int quality = 0;
        if (item.getSellIn() <= 0) { // expired
            quality = 2;
        } else {
            quality = 1;
        }
        item.setQuality(item.getQuality() + quality);
    }

    /**
     * Update Backstage pass item.
     * @param item instance of backstage pass
     */
    protected void updateBackstagePass(ItemBean item) {
        if (item.getSellIn() == 0) { // expired
            item.setQuality(0);
        } else if (item.getSellIn() <= 5) { // 5 or less days -> +3
            item.setQuality(item.getQuality() + 3);
        } else if (item.getSellIn() <= 10) { // 10 or less days -> +2
            item.setQuality(item.getQuality() + 2);
        } else {
            item.setQuality(item.getQuality() + 1);
        }
    }

    /**
     * Update norma item.
     * @param item instance of normal item
     */
    protected void updateNormalItem(ItemBean item) {
        int quality = 0;
        if (item.getSellIn() <= 0) { // expired
            quality = 2;
        } else {
            quality = 1;
        }

        item.setQuality(item.getQuality() - quality);
    }

    protected void updateConjured(ItemBean item) {
        int quality = 0;
        if (item.getSellIn() <= 0) {
            quality = 4;
        } else {
            quality = 2;
        }
        item.setQuality(item.getQuality() - quality);
    }

    /**
     * Updates item's quality and sellin
     * @param item any item that needs to be updated
     */
    protected void updateItem(ItemBean item) {
        // sulfuras excluded from update
        if (item.getName().equals("Sulfuras, Hand of Ragnaros")) {
            return;
        }

        if (item.getName().startsWith("Conjured")) {
            updateConjured(item);
        } else if (item.getName().equals("Aged Brie")) {
            updateAgedBrie(item);
        } else if (item.getName().equals("Backstage passes to a TAFKAL80ETC concert")) {
            updateBackstagePass(item);
        } else {
            updateNormalItem(item);
        }

        // sellin update
        item.setSellIn(item.getSellIn() - 1);

        if (item.getQuality() > 50) {
            item.setQuality(50);
        }

        if (item.getQuality() < 0) {
            item.setQuality(0);
        }
    }

    public void updateQuality() {
        items.parallelStream()
             .forEach(item -> updateItem(item));
    }
}